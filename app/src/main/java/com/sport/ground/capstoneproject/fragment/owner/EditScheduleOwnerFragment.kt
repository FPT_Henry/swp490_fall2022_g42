package com.sport.ground.capstoneproject.fragment.owner

import android.app.DatePickerDialog
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.Fragment
import com.sport.ground.capstoneproject.R
import com.sport.ground.capstoneproject.databinding.FragmentEditScheduleOwnerBinding
import com.sport.ground.capstoneproject.fragment.adapter.ServiceDetailBySlotAdapter
import com.sport.ground.capstoneproject.fragment.adapter.ServiceSetAdapter
import com.sport.ground.capstoneproject.fragment.adapter.TimeSetAdapter
import com.sport.ground.capstoneproject.fragment.ownerinterface.IOnClickEditServiceItem
import com.sport.ground.capstoneproject.fragment.ownerinterface.IOnClickServiceItem
import com.sport.ground.capstoneproject.model.BookedGround
import com.sport.ground.capstoneproject.model.Schedule
import com.sport.ground.capstoneproject.model.Service
import org.w3c.dom.Text
import java.util.*


class EditScheduleOwnerFragment : Fragment(), IOnClickServiceItem, IOnClickEditServiceItem {

    private var _binding: FragmentEditScheduleOwnerBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentEditScheduleOwnerBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val bookedGroundArgs = this.arguments
        val scheduleDetail: BookedGround? = bookedGroundArgs?.getParcelable("booked_ground")
        setData(scheduleDetail)
        binding.btnHourSelectedEditSchedule.setOnClickListener {
            openDialogEditHourSchedule(Gravity.CENTER, scheduleDetail)
        }

        binding.btnServiceSelectedEditSchedule.setOnClickListener {
            openDialogEditServiceSchedule(Gravity.CENTER, scheduleDetail)
        }

        binding.btnDayEditSchedule.setOnClickListener {
            setDateView()
        }

        setUpCheckbox()

    }

    private fun setUpCheckbox() {
        binding.cbPaymentAllEditSchedule.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                !binding.cbPaymentHalfEditSchedule.isChecked
            }
        }
        binding.cbPaymentHalfEditSchedule.setOnCheckedChangeListener { buttonView, isChecked ->
            !binding.cbPaymentAllEditSchedule.isChecked
        }
    }

    private var tvTotal: TextView? = null
    private fun openDialogEditDaySchedule(center: Int, position: Int) {
        val filterDialog = Dialog(requireContext())
        filterDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        filterDialog.setContentView(R.layout.custom_dialog_edit_time_detail_schedule)
        val window = filterDialog.window ?: return
        window.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val windowAttributes = window.attributes
        windowAttributes.gravity = center
        window.attributes = windowAttributes
        if (Gravity.CENTER == center) {
            filterDialog.setCancelable(true)
        } else {
            filterDialog.setCancelable(false)
        }

        val btnSave =
            filterDialog.findViewById<AppCompatButton>(R.id.btn_save_service_edit_detail_schedule)
        val lvServiceDetailEdit =
            filterDialog.findViewById<ListView>(R.id.lv_service_edit_detail_schedule)
        tvTotal = filterDialog.findViewById(R.id.tv_total_price_selected)

        lvServiceDetailEdit.adapter = ServiceDetailBySlotAdapter(this, getService())


        btnSave.setOnClickListener {
            filterDialog.dismiss()
        }

        filterDialog.show()
    }

    private fun setDateView() {
        val calendar = Calendar.getInstance()
        val year = calendar.get((Calendar.YEAR))
        val month = calendar.get((Calendar.MONTH))
        val day = calendar.get((Calendar.DAY_OF_MONTH))
        val dateTime = StringBuilder()
        val dpd = DatePickerDialog(requireContext(), { _, year, monthOfYear, dayOfMonth ->
            binding.tvShowDateEditSchedule.text =
                dateTime.append(dayOfMonth).append("/").append(monthOfYear + 1).append("/")
                    .append(year)
        }, year, month, day)
        dpd.show()
    }

    private fun openDialogEditServiceSchedule(center: Int, scheduleDetail: BookedGround?) {
        val filterDialog = Dialog(requireContext())
        filterDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        filterDialog.setContentView(R.layout.custom_dialog_edit_service_schedule)
        val window = filterDialog.window ?: return
        window.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val windowAttributes = window.attributes
        windowAttributes.gravity = center
        window.attributes = windowAttributes
        if (Gravity.CENTER == center) {
            filterDialog.setCancelable(true)
        } else {
            filterDialog.setCancelable(false)
        }
        val btnSave =
            filterDialog.findViewById<AppCompatButton>(R.id.btn_save_service_edit_schedule)
        val lvItem = filterDialog.findViewById<ListView>(R.id.lv_detail_service_edit_schedule)

        val slots = mutableListOf<String>()
        getSchedules().forEach {
            if (scheduleDetail?.day == it.day) {
                if (it.isBooking) {
                    slots.add(it.timeSlot)
                }
            }
        }
        lvItem.adapter = ServiceSetAdapter(this, slots)

        btnSave.setOnClickListener {
            filterDialog.dismiss()
        }

        filterDialog.show()
    }

    private fun openDialogEditHourSchedule(center: Int, scheduleDetail: BookedGround?) {
        val filterDialog = Dialog(requireContext())
        filterDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        filterDialog.setContentView(R.layout.custom_dialog_edit_time_schedule)
        val window = filterDialog.window ?: return
        window.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val windowAttributes = window.attributes
        windowAttributes.gravity = center
        window.attributes = windowAttributes
        if (Gravity.CENTER == center) {
            filterDialog.setCancelable(true)
        } else {
            filterDialog.setCancelable(false)
        }

        val btnSave = filterDialog.findViewById<AppCompatButton>(R.id.btn_save_hour_edit_schedule)
        val lvItem = filterDialog.findViewById<ListView>(R.id.lv_detail_hour_edit_schedule)

        val slots = mutableListOf<Schedule>()
        getSchedules().forEach {
            if (scheduleDetail?.day == it.day) {
                slots.add(it)
            }
        }
        lvItem.adapter = TimeSetAdapter(requireContext(), slots)

        btnSave.setOnClickListener {
            filterDialog.dismiss()
        }

        filterDialog.show()
    }

    private fun setData(scheduleDetail: BookedGround?) {
        val isBookType = scheduleDetail?.bookType
        binding.edtNameEditSchedule.setText(scheduleDetail?.renterName.toString())
        binding.edtPhoneEditSchedule.setText(scheduleDetail?.phone.toString())
        binding.edtTotalEditSchedule.setText(scheduleDetail?.totalPrice.toString())
        binding.edtNoteEditSchedule.setText(scheduleDetail?.note.toString())
        binding.tvShowDateEditSchedule.text = scheduleDetail?.day.toString()
        if (isBookType == true) {
            binding.cbPaymentAllEditSchedule.isChecked
            !binding.cbPaymentHalfEditSchedule.isChecked
        } else {
            binding.cbPaymentHalfEditSchedule.isChecked
            !binding.cbPaymentAllEditSchedule.isChecked
        }

        val adapterManager = ArrayAdapter(
            requireContext(),
            R.layout.custom_text_spinner_item, getPaymentType()
        )
        binding.spnPaymentTypeEditSchedule.adapter = adapterManager

    }

    private fun getPaymentType(): List<String> {
        return listOf("VNPay", "Momo")
    }

    override fun onDestroy() {
        _binding = null
        super.onDestroy()
    }

    private fun getService(): List<Service> {
        return mutableListOf(
            Service(1, "Nước uống", 100000f),
            Service(2, "Khăn lau", 100000f),
            Service(3, "Nước chanh", 100000f),
            Service(4, "Nước suối", 100000f),
            Service(5, "Nước ngọt", 100000f),
        )
    }

    private fun getSchedules(): List<Schedule> {
        return mutableListOf(
            Schedule(
                1,
                "8:00 - 9:30",
                false,
                "10/10/2022",
                "Sân bóng đá",
                100000f
            ),
            Schedule(
                2,
                "9:30 - 11:00",
                true,
                "10/10/2022",
                "Sân bóng đá",
                100000f
            ),
            Schedule(
                3,
                "11:00 - 12:30",
                true,
                "10/10/2022",
                "Sân bóng đá",
                100000f

            ),
            Schedule(
                4,
                "12:30 - 14:00",
                true,
                "10/10/2022",
                "Sân bóng đá",
                100000f
            ), Schedule(
                5,
                "14:00 - 15:30",
                false,
                "10/10/2022",
                "Sân bóng đá",
                100000f
            ), Schedule(
                6,
                "8:00 - 9:30",
                true,
                "11/10/2022", "Sân bóng chuyền",
                100000f
            ),
            Schedule(
                7,
                "9:30 - 11:00",
                false,
                "11/10/2022", "Sân bóng chuyền",
                100000f
            ),
            Schedule(
                8,
                "11:00 - 12:30",
                false,
                "11/10/2022", "Sân bóng chuyền",
                100000f
            ),
            Schedule(
                9,
                "12:30 - 14:00",
                false,
                "11/10/2022", "Sân bóng chuyền",
                100000f
            ), Schedule(
                10,
                "14:00 - 15:30",
                true,
                "11/10/2022", "Sân bóng chuyền",
                100000f
            ), Schedule(
                11,
                "8:00 - 9:30",
                true,
                "12/10/2022", "Sân bóng chuyền1",
                100000f
            ),
            Schedule(
                12,
                "9:30 - 11:00",
                false,
                "12/10/2022", "Sân bóng chuyền1",
                100000f
            ),
            Schedule(
                13,
                "11:00 - 12:30",
                false,
                "12/10/2022", "Sân bóng chuyền1",
                100000f
            ),
            Schedule(
                14,
                "12:30 - 14:00",
                false,
                "12/10/2022", "Sân bóng chuyền1",
                100000f
            ), Schedule(
                15,
                "14:00 - 15:30",
                true,
                "12/10/2022", "Sân bóng chuyền1",
                100000f
            )


        )
    }

    override fun itemCallbackService(position: Int) {
        openDialogEditDaySchedule(Gravity.CENTER, position)
    }

    override fun decreaseAmountCallBack(position: Int, edtAmount: EditText, service: Service) {
        val edtNumber = edtAmount.text.toString()
        var number = edtNumber.toInt()
        number--
        if (number < 0) {
            return
        }
        edtAmount.setText(number.toString())
        tvTotal?.text = (number * service.servicePrice).toString()
    }

    override fun increaseAmountCallBack(position: Int, edtAmount: EditText, service: Service) {
        val edtNumber = edtAmount.text.toString()
        var number = edtNumber.toInt()
        number++
        if (number >= 50) {
            return
        }
        edtAmount.setText(number.toString())
        tvTotal?.text = (number * service.servicePrice).toString()
    }
}