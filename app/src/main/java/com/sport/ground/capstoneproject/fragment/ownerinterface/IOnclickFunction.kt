package com.sport.ground.capstoneproject.fragment.ownerinterface

interface IOnclickFunction {
    fun itemCallbackFunction(position: Int)
}