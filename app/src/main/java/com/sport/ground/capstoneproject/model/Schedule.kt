package com.sport.ground.capstoneproject.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
data class Schedule(
    val scheduleId: Int,
    val timeSlot: String,
    val isBooking: Boolean,
    val day:String,
    val categoryName:String,
    val price: Float
) : Parcelable, Serializable