package com.sport.ground.capstoneproject.fragment.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.sport.ground.capstoneproject.R
import com.sport.ground.capstoneproject.model.SportGround

class SportGroundAdapter(
    private var context: Context,
    private var grounds: List<SportGround>
) : BaseAdapter() {

    override fun getCount(): Int = grounds.size

    override fun getItem(position: Int): Any = grounds[position]

    override fun getItemId(position: Int): Long = grounds[position].groundId.toLong()

    @SuppressLint("SetTextI18n")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        var view: View? = null
        if (view == null) {
            view =
                LayoutInflater.from(context).inflate(R.layout.item_sport_ground_owner, parent, false)
        }
        val currentGround = getItem(position) as SportGround

        val tvGroundName: TextView? = view?.findViewById(R.id.tv_ground_name)
        val tvGroundAddress: TextView? = view?.findViewById(R.id.tv_ground_address)
        val tvGroundSlotEmp: TextView? = view?.findViewById(R.id.tv_ground_slot_empty)
        val imgGround: ImageView? = view?.findViewById(R.id.img_ground)
        tvGroundName?.text = currentGround.groundName
        tvGroundAddress?.text = currentGround.address
        tvGroundSlotEmp?.text = "${currentGround.slot} ca"
        imgGround?.setImageResource(currentGround.image)
        return view
    }


}




