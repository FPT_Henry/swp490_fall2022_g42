package com.sport.ground.capstoneproject.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.sport.ground.capstoneproject.R
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        InitAction()
    }

    private fun InitAction() {
        btn_welcome_login.setOnClickListener {
            val action =
                HomeFragmentDirections.actionHomeFragmentToRegisterAccountWithRoleFragment()
            findNavController().navigate(action)
        }

        btn_login.setOnClickListener {
            val action1 = HomeFragmentDirections.actionHomeFragmentToLoginFragment()
            findNavController().navigate(action1)
        }
    }
}

