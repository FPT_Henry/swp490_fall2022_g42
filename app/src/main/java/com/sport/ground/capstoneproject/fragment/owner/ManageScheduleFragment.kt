package com.sport.ground.capstoneproject.fragment.owner

import android.app.DatePickerDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.sport.ground.capstoneproject.R
import com.sport.ground.capstoneproject.databinding.FragmentManageScheduleBinding
import com.sport.ground.capstoneproject.fragment.adapter.ScheduleAdapter
import com.sport.ground.capstoneproject.fragment.ownerinterface.IOnclickScheduleDetail
import com.sport.ground.capstoneproject.model.BookedGround
import com.sport.ground.capstoneproject.model.DayGround
import com.sport.ground.capstoneproject.model.Schedule
import java.util.*

class ManageScheduleFragment : Fragment(), IOnclickScheduleDetail {

    private var _binding: FragmentManageScheduleBinding? = null
    private val binding get() = _binding!!

    private lateinit var schedules: List<Schedule>
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentManageScheduleBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        schedules = getSchedules()
        binding.lvSchedule.adapter = ScheduleAdapter(schedules, this)
        setGroundToSpinner()
        binding.btnDate.setOnClickListener {
            setDateView()
        }
        binding.tvDate.text = "10/10/2022"
        setDefaultSchedule()
        scheduleForEachDay()
    }

    private fun setDefaultSchedule() {
        val schedules = mutableListOf<Schedule>()
        val categoryValue = binding.spinnerGroundName.selectedItem.toString()
        getSchedules().forEach { schedule ->
            if (schedule.day == binding.tvDate.text.toString() && schedule.categoryName == categoryValue) {
                schedules.add(schedule)
            }
        }
        binding.lvSchedule.adapter = ScheduleAdapter(schedules, this)
    }

    private fun scheduleForEachDay() {
        binding.btnShowSchedule.setOnClickListener {
            val categoryValue = binding.spinnerGroundName.selectedItem.toString()
            val schedules = mutableListOf<Schedule>()
            getSchedules().forEach { schedule ->
                if (schedule.day == binding.tvDate.text.toString() && schedule.categoryName == categoryValue) {
                    schedules.add(schedule)
                }
            }
            binding.lvSchedule.adapter = ScheduleAdapter(schedules, this)
        }
    }

    private fun setGroundToSpinner() {
        val spinnerCategories = arrayOf(
            "Sân bóng đá", "Sân bóng chuyền", "Sân bóng chuyền1"
        )
        val adapterCategory = ArrayAdapter(
            requireContext(),
            R.layout.custom_text_spinner_item, spinnerCategories
        )
        binding.spinnerGroundName.adapter = adapterCategory
    }


    private fun setDateView() {
        val calendar = Calendar.getInstance()
        val year = calendar.get((Calendar.YEAR))
        val month = calendar.get((Calendar.MONTH))
        val day = calendar.get((Calendar.DAY_OF_MONTH))
        val dateTime = StringBuilder()
        val dpd = DatePickerDialog(requireContext(), { _, year, monthOfYear, dayOfMonth ->
            binding.tvDate.text =
                dateTime.append(dayOfMonth).append("/").append(monthOfYear + 1).append("/")
                    .append(year)
        }, year, month, day)
        dpd.show()
    }

    private fun getScheduleEachDay(): List<DayGround> {
        return mutableListOf(
            DayGround(1, "10/10/2022"),
            DayGround(2, "11/10/2022"),
            DayGround(3, "12/10/2022"),
        )
    }


    private fun getSchedules(): List<Schedule> {
        return mutableListOf(
            Schedule(
                1,
                "8:00 - 9:30",
                false,
                "10/10/2022",
                "Sân bóng đá",
                100000f
            ),
            Schedule(
                2,
                "9:30 - 11:00",
                true,
                "10/10/2022",
                "Sân bóng đá",
                100000f
            ),
            Schedule(
                3,
                "11:00 - 12:30",
                true,
                "10/10/2022",
                "Sân bóng đá",
                100000f

            ),
            Schedule(
                4,
                "12:30 - 14:00",
                true,
                "10/10/2022",
                "Sân bóng đá",
                100000f
            ), Schedule(
                5,
                "14:00 - 15:30",
                false,
                "10/10/2022",
                "Sân bóng đá",
                100000f
            ), Schedule(
                6,
                "8:00 - 9:30",
                true,
                "11/10/2022", "Sân bóng chuyền",
                100000f
            ),
            Schedule(
                7,
                "9:30 - 11:00",
                false,
                "11/10/2022", "Sân bóng chuyền",
                100000f
            ),
            Schedule(
                8,
                "11:00 - 12:30",
                false,
                "11/10/2022", "Sân bóng chuyền",
                100000f
            ),
            Schedule(
                9,
                "12:30 - 14:00",
                false,
                "11/10/2022", "Sân bóng chuyền",
                100000f
            ), Schedule(
                10,
                "14:00 - 15:30",
                true,
                "11/10/2022", "Sân bóng chuyền",
                100000f
            ), Schedule(
                11,
                "8:00 - 9:30",
                true,
                "12/10/2022", "Sân bóng chuyền1",
                100000f
            ),
            Schedule(
                12,
                "9:30 - 11:00",
                false,
                "12/10/2022", "Sân bóng chuyền1",
                100000f
            ),
            Schedule(
                13,
                "11:00 - 12:30",
                false,
                "12/10/2022", "Sân bóng chuyền1",
                100000f
            ),
            Schedule(
                14,
                "12:30 - 14:00",
                false,
                "12/10/2022", "Sân bóng chuyền1",
                100000f
            ), Schedule(
                15,
                "14:00 - 15:30",
                true,
                "12/10/2022", "Sân bóng chuyền1",
                100000f
            )


        )
    }

    override fun itemCallbackScheduleDetail(scheduleId: Int) {
        Toast.makeText(requireContext(), "$scheduleId", Toast.LENGTH_SHORT).show()
        val schedule = getSchedules().find {
            it.scheduleId == scheduleId
        }
        if (schedule?.isBooking == false) {
            Toast.makeText(requireContext(), "Sân chưa được đặt", Toast.LENGTH_SHORT).show()
            return
        }
        val bundle = Bundle()
        bundle.putParcelable("schedule", schedule)
        val scheduleDetailFragment = ScheduleDetailFragment()
        scheduleDetailFragment.arguments = bundle
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.replace(R.id.home_owner_container, scheduleDetailFragment, "TAG")
            ?.addToBackStack("TAG")?.commit()
    }


    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}