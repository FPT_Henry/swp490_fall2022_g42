package com.sport.ground.capstoneproject.`interface`

import com.sport.ground.capstoneproject.model.User
import com.sport.ground.capstoneproject.model.UserLogin
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface ApiInterface {
    //truyền domain của api
    //http://apilayer.net/api/live?access_key=843d4d34ae72b3882e3db642c51e28e6&currencies=VND&source=USD&format=1


    @POST("sign_up")
    fun registerUsers(@Body user: User): Call<User>

    @POST("sign_in")
    fun loginUser(@Body userLogin: UserLogin): Call<UserLogin>

    @GET("api/live")
    fun converUsDtoVND(
        @Query("access_key") access_key: String,
        @Query("currencies") currencies: String,
        @Query("source") source: String,
        @Query("format") format: Int
    )
}

