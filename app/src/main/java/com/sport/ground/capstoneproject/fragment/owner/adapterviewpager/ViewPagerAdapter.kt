package com.sport.ground.capstoneproject.fragment.owner.adapterviewpager

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.sport.ground.capstoneproject.fragment.owner.*


class ViewPagerAdapter(fragmentActivity: FragmentActivity) :
    FragmentStateAdapter(fragmentActivity) {

    companion object {
        const val NUM_PAGES = 5
    }

    override fun getItemCount(): Int = NUM_PAGES
    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> HomeOwnerFragment()
            1 -> HistoryOwnerFragment()
            2 -> ChatOwnerFragment()
            3 -> NotificationOwnerFragment()
            4 -> ProfileOwnerFragment()
            else -> HomeOwnerFragment()
        }
    }


}