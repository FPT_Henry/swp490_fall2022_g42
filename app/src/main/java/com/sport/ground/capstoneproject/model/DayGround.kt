package com.sport.ground.capstoneproject.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
data class DayGround(
    val dayId: Int,
    val Day: String,
) : Parcelable, Serializable