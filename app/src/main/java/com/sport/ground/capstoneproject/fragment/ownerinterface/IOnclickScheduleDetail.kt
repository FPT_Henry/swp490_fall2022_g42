package com.sport.ground.capstoneproject.fragment.ownerinterface

interface IOnclickScheduleDetail {
    fun itemCallbackScheduleDetail(scheduleId: Int)
}