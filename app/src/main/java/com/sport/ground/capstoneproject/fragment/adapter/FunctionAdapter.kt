package com.sport.ground.capstoneproject.fragment.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView.OnItemClickListener
import androidx.appcompat.widget.AppCompatButton
import androidx.recyclerview.widget.RecyclerView
import com.sport.ground.capstoneproject.R
import com.sport.ground.capstoneproject.model.Function
import com.sport.ground.capstoneproject.fragment.ownerinterface.IOnclickFunction

class FunctionAdapter(
    private val functions: List<Function>,
    private val onItemClickListener: IOnclickFunction
) :
    RecyclerView.Adapter<FunctionAdapter.ViewHolder>() {


    class ViewHolder(view: View, onItemClickListener: IOnclickFunction) :
        RecyclerView.ViewHolder(view) {
        val btnFunction: AppCompatButton

        init {
            btnFunction = view.findViewById(R.id.btn_function_item)
            btnFunction.setOnClickListener {
                onItemClickListener.itemCallbackFunction(adapterPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_function_owner, parent, false)
        return ViewHolder(view, onItemClickListener)
    }


    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.btnFunction.text = functions[position].functionString

    }


    override fun getItemCount(): Int = functions.size
}

