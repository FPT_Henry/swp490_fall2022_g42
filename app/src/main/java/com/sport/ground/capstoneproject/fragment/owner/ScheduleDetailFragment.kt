package com.sport.ground.capstoneproject.fragment.owner

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.ListView
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.Fragment
import com.sport.ground.capstoneproject.Object
import com.sport.ground.capstoneproject.R
import com.sport.ground.capstoneproject.databinding.FragmentScheduleDetailBinding
import com.sport.ground.capstoneproject.fragment.adapter.ServiceDetailAdapter
import com.sport.ground.capstoneproject.fragment.adapter.SportGroundAdapter
import com.sport.ground.capstoneproject.model.BookedGround
import com.sport.ground.capstoneproject.model.Schedule
import com.sport.ground.capstoneproject.model.ServiceDetail
import com.sport.ground.capstoneproject.model.SportGround

class ScheduleDetailFragment : Fragment() {

    private var _binding: FragmentScheduleDetailBinding? = null
    private val binding get() = _binding!!

    private var bookedGround: BookedGround? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentScheduleDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val scheduleArgs = this.arguments
        val scheduleDetail: Schedule? = scheduleArgs?.getParcelable("schedule")
        setData(scheduleDetail)
        binding.btnDetailService.setOnClickListener {
            openDetailServiceDialog(Gravity.CENTER, scheduleDetail)
        }
        binding.btnEditDetail.setOnClickListener {
            openEditSchedule()
        }
    }

    private fun openEditSchedule() {

        val editFragment = EditScheduleOwnerFragment()
        val bundle = Bundle()
        Log.e("HoangPV", ": $bookedGround")
        bundle.putParcelable("booked_ground", bookedGround)
        editFragment.arguments = bundle
        val transaction = requireActivity().supportFragmentManager.beginTransaction()
        transaction.replace(R.id.home_owner_container, editFragment, "TAG")
            .addToBackStack("TAG").commit()
    }

    private fun openDetailServiceDialog(center: Int, scheduleDetail: Schedule?) {
        val filterDialog = Dialog(requireContext())
        filterDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        filterDialog.setContentView(R.layout.custom_dialog_service_detail)
        val window = filterDialog.window ?: return
        window.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val windowAttributes = window.attributes
        windowAttributes.gravity = center
        window.attributes = windowAttributes
        if (Gravity.CENTER == center) {
            filterDialog.setCancelable(true)
        } else {
            filterDialog.setCancelable(false)
        }
        var serviceDetail: ServiceDetail? = null
        getListBookedService().forEach {
            if (scheduleDetail?.scheduleId == it.bookedGroundId) {
                serviceDetail = it
            }
        }

        val services = serviceDetail?.services
        Log.e("HoangPV123", "$services")

        val lvServiceDetail = filterDialog.findViewById<ListView>(R.id.lv_detail_service)
        val btnBackServiceDetail =
            filterDialog.findViewById<AppCompatButton>(R.id.btn_back_service_detail)
        lvServiceDetail.adapter = services?.let { ServiceDetailAdapter(requireContext(), it) }
        btnBackServiceDetail.setOnClickListener {
            filterDialog.dismiss()
        }


        filterDialog.show()
    }

    private fun setData(scheduleDetail: Schedule?) {
        Log.e("HoangPV", "$scheduleDetail: ")

        listGroundBookedInfo().forEach {
            val bookType = if (it.bookType) "Toàn bộ" else "Đặt cọc"
            if (it.day == scheduleDetail?.day && it.time_slot == scheduleDetail.timeSlot) {
                bookedGround = it
                binding.edtRenterNameDetail.setText(it.renterName)
                binding.edtRenterPhoneDetail.setText(it.phone)
                binding.edtRenterDayDetail.setText(it.day)
                binding.edtBookType.setText(bookType)
                binding.edtTotalAmountDetail.setText(it.totalPrice.toString())
                binding.edtPaymentTypeDetail.setText(it.paymentType)
                binding.edtNoteDetail.setText(it.note)
            }
        }
    }


    private fun getListBookedService(): List<ServiceDetail> {
        return listOf(
            ServiceDetail(1, 1, listOf("Nước chanh", "khăn lau")),
            ServiceDetail(2, 2, listOf("Nước chanh2", "khăn lau2")),
            ServiceDetail(3, 3, listOf("Nước chanh3", "khăn lau3")),
            ServiceDetail(4, 5, listOf("Nước chanh4", "khăn lau4")),
            ServiceDetail(5, 5, listOf("Nước chanh5", "khăn lau5")),
            ServiceDetail(6, 6, listOf("Nước chanh6", "khăn lau6"))
        )
    }


    private fun listGroundBookedInfo(): List<BookedGround> {
        return listOf(
            BookedGround(
                1,
                "10/10/2022",
                "8:00 - 9:30",
                "Hoangpv1",
                "0123456789",
                1,
                false,
                1000f,
                "VN Pay",
                "123456"
            ), BookedGround(
                2,
                "10/10/2022",
                "9:30 - 11:00",
                "Hoangpv2",
                "0123456789",
                1,
                true,
                10030f,
                "VN Pay",
                "123456"
            ), BookedGround(
                3,
                "11/10/2022",
                "11:00 - 12:30",
                "Hoangpv3",
                "0123456789",
                1,
                false,
                10030f,
                "VN Pay",
                "123456"
            ), BookedGround(
                4,
                "11/10/2022",
                "12:30 - 14:00",
                "Hoangpv4",
                "0123456789",
                1,
                false,
                10030f,
                "VN Pay",
                "123456"
            ), BookedGround(
                5,
                "12/10/2022",
                "14:00 - 15:30",
                "Hoangpv5",
                "0123456789",
                1,
                true,
                10030f,
                "VN Pay",
                "123456"
            ), BookedGround(
                6,
                "12/10/2022",
                "8:00 - 9:30",
                "Hoangpv6",
                "0123456789",
                1,
                false,
                10030f,
                "VN Pay",
                "123456"
            )


        )
    }
}