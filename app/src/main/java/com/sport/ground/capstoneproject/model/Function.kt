package com.sport.ground.capstoneproject.model


import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
data class Function(
    val functionId: Int,
    val functionString: String
) : Parcelable, Serializable