package com.sport.ground.capstoneproject.fragment.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import com.sport.ground.capstoneproject.R
import com.sport.ground.capstoneproject.fragment.ownerinterface.IOnClickEditServiceItem
import com.sport.ground.capstoneproject.model.Service

class ServiceDetailBySlotAdapter(
    private var iOnClickEditItemService: IOnClickEditServiceItem,
    private var services: List<Service>
) : BaseAdapter() {

    override fun getCount(): Int = services.size


    override fun getItem(position: Int): Any = services[position]

    override fun getItemId(position: Int): Long = services[position].serviceId.toLong()


    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        var view: View? = null
        if (view == null) {
            view =
                LayoutInflater.from(parent?.context)
                    .inflate(R.layout.item_edit_service_detail, parent, false)
        }
        val service = getItem(position) as Service

        val tvService: TextView? = view?.findViewById(R.id.tv_service_name_item_schedule)
        val btnDecrease: AppCompatButton? = view?.findViewById(R.id.btn_decrease_amount)
        val btnIncrease: AppCompatButton? = view?.findViewById(R.id.btn_increase_amount)
        val tvPrice: TextView? = view?.findViewById(R.id.tv_service_price_item_Schedule)
        val edtAmount: EditText? = view?.findViewById(R.id.edt_service_item_schedule)
        tvService?.text = service.serviceName
        tvPrice?.text = "${service.servicePrice} VND"
        btnDecrease?.setOnClickListener {
            if (edtAmount != null) {
                iOnClickEditItemService.decreaseAmountCallBack(position, edtAmount, service)
            }
        }
        btnIncrease?.setOnClickListener {
            if (edtAmount != null) {
                iOnClickEditItemService.increaseAmountCallBack(position, edtAmount, service)
            }
        }
        return view
    }

}