package com.sport.ground.capstoneproject.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
data class ServiceDetail(
    val serviceId: Int,
    val bookedGroundId: Int,
    val services: List<String>,
) : Parcelable, Serializable