package com.sport.ground.capstoneproject.fragment.ownerinterface

interface IOnClickServiceItem {
    fun itemCallbackService(position: Int)
}