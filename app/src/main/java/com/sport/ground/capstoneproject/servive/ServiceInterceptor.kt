package com.sport.ground.capstoneproject.servive

import okhttp3.Interceptor
import okhttp3.Response

class ServiceInterceptor : Interceptor {

    var accessToken : String = "";

    fun Token(token: String ) {
        this.accessToken = token;
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()

        if(request.header("No-Authentication")==null){
            //val token = getTokenFromSharedPreference();
            //or use Token Function
            if(!accessToken.isNullOrEmpty())
            {
                val finalToken =  "Bearer "+accessToken
                request = request.newBuilder()
                    .addHeader("Authorization",finalToken)
                    .build()
            }

        }

        return chain.proceed(request)
    }

}