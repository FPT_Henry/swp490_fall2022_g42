package com.sport.ground.capstoneproject.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
data class BookedGround(
    val bookedId: Int,
    val day: String,
    val time_slot: String,
    val renterName: String,
    val phone: String,
    val serviceId: Int,
    val bookType: Boolean,
    val totalPrice: Float,
    val paymentType: String,
    val note: String
) : Parcelable, Serializable