package com.sport.ground.capstoneproject.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
data class Service(
    val serviceId: Int,
    val serviceName: String,
    val servicePrice: Float,
) : Parcelable, Serializable
