package com.sport.ground.capstoneproject.fragment.owner


import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.SwitchCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.sport.ground.capstoneproject.Object
import com.sport.ground.capstoneproject.R
import com.sport.ground.capstoneproject.databinding.FragmentHomeOwnerBinding
import com.sport.ground.capstoneproject.fragment.adapter.FunctionAdapter
import com.sport.ground.capstoneproject.fragment.adapter.SportGroundAdapter
import com.sport.ground.capstoneproject.fragment.ownerinterface.IOnclickFunction
import com.sport.ground.capstoneproject.model.Function
import com.sport.ground.capstoneproject.model.SportGround


class HomeOwnerFragment : Fragment(), IOnclickFunction {

    private var _binding: FragmentHomeOwnerBinding? = null
    private val binding get() = _binding!!

    private lateinit var grounds: List<SportGround>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeOwnerBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        grounds = getListGround()
        binding.listGrounds.adapter = SportGroundAdapter(requireContext(), grounds)
        val layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        binding.rcvFunction.layoutManager = layoutManager
        val functionAdapter = FunctionAdapter(getListFunction(), this)

        binding.rcvFunction.adapter = functionAdapter
        binding.tvTotalAmount.text = grounds.size.toString()

        isNullGround(grounds)
        binding.imgFilter.setOnClickListener {
            openFilterDialog(Gravity.RIGHT)
        }
    }

    private fun openFilterDialog(gravity: Int) {
        var isEnableFilter = false
        val filterDialog = Dialog(requireContext())
        filterDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        filterDialog.setContentView(R.layout.custom_dialog_filter_owner)
        val window = filterDialog.window ?: return
        window.setLayout(
            Object.LAYOUT_WITH,
            Object.LAYOUT_HEIGHT
        )
        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val windowAttributes = window.attributes
        windowAttributes.gravity = Gravity.RIGHT
        window.attributes = windowAttributes
        if (Gravity.RIGHT == gravity) {
            filterDialog.setCancelable(true)
        } else {
            filterDialog.setCancelable(false)
        }
        val spinnerCategory = filterDialog.findViewById<Spinner>(R.id.spinner_category_ground)
        val spinnerManager = filterDialog.findViewById<Spinner>(R.id.spinner_manager)
        val switchGroundEmpty = filterDialog.findViewById<SwitchCompat>(R.id.switch_ground_empty)
        val buttonReset = filterDialog.findViewById<AppCompatButton>(R.id.btn_reset_filter_owner)
        val buttonApply = filterDialog.findViewById<AppCompatButton>(R.id.btn_apply_owner)
        setDataToSpinner(spinnerCategory, spinnerManager)
        switchGroundEmpty.setOnCheckedChangeListener { _, isChecked ->
            isEnableFilter = isChecked
        }
        buttonReset.setOnClickListener {
            spinnerCategory.setSelection(0)
            spinnerManager.setSelection(0)
            switchGroundEmpty.isChecked = false
        }

        buttonApply.setOnClickListener {
            val groundFilter = mutableListOf<SportGround>()
            val categoryValue = spinnerCategory.selectedItem.toString()
            val managerValue = spinnerManager.selectedItem.toString()


            if (isEnableFilter) {
                grounds.forEach {
                    if (it.manager == managerValue && it.groundType == categoryValue && it.slot > 0) {
                        groundFilter.add(it)
                    }
                }
            } else {
                grounds.forEach {
                    if (it.manager == managerValue && it.groundType == categoryValue) {
                        groundFilter.add(it)
                    }
                }
            }
            isNullGround(groundFilter)
            binding.tvTotalAmount.text = groundFilter.size.toString()
            binding.listGrounds.adapter = SportGroundAdapter(requireContext(), groundFilter)

            filterDialog.dismiss()
        }

        filterDialog.show()
    }


    private fun setDataToSpinner(spinnerCategory: Spinner, spinnerManager: Spinner) {
        val spinnerCategories = arrayOf(
            "Sân bóng đá", "Sân bóng chuyền", "Sân bóng chuyền1"
        )
        val spinnerManagers = mutableListOf<String>()
        grounds.forEach {
            spinnerManagers.add(it.manager)
        }
        val adapterCategory = ArrayAdapter(
            requireContext(),
            R.layout.custom_text_spinner_item, spinnerCategories
        )
        val adapterManager = ArrayAdapter(
            requireContext(),
            R.layout.custom_text_spinner_item, spinnerManagers
        )
        spinnerCategory.adapter = adapterCategory
        spinnerManager.adapter = adapterManager

    }

    private fun isNullGround(grounds: List<SportGround>) {
        if (grounds.isEmpty()) {
            binding.tvNull.visibility = View.VISIBLE
            binding.tvNull.text = "Không có sân nào!!"
        } else {
            binding.tvNull.visibility = View.GONE
        }
    }

    private fun getListGround(): List<SportGround> {
        return mutableListOf(
            SportGround(
                1,
                "Sân hoà lạc",
                "Sân bóng đá",
                "Hoà lạc",
                "hoang1",
                R.drawable.image6,
                1
            ),
            SportGround(
                2,
                "Sân thạch thất",
                "Sân bóng chuyền",
                " hoà lạc",
                "hoang2",
                R.drawable.image6,
                1
            ),
            SportGround(
                3,
                "Sân thạch thất1",
                "Sân bóng chuyền1",
                " hoà lạc1",
                "hoang2",
                R.drawable.image6,
                2
            ),
            SportGround(
                3,
                "Sân thạch thất1",
                "Sân bóng chuyền1",
                " hoà lạc1",
                "hoang2",
                R.drawable.image6,
                2
            ),
            SportGround(
                3,
                "Sân thạch thất1",
                "Sân bóng chuyền1",
                " hoà lạc1",
                "hoang5",
                R.drawable.image6,
                2
            ),
            SportGround(
                3,
                "Sân thạch thất1",
                "Sân bóng chuyền1",
                " hoà lạc1",
                "hoang6",
                R.drawable.image6,
                0
            ),
            SportGround(
                3,
                "Sân thạch thất1",
                "Sân bóng chuyền1",
                " hoà lạc1",
                "hoang7",
                R.drawable.image6,
                0
            )
        )
    }

    private fun getListFunction(): List<Function> {
        return listOf(
            Function(0, "Thống kê"),
            Function(1, "Quản lý lịch"),
            Function(2, "Tài khoản"),
            Function(3, "Tìm kiếm")
        )

    }


    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun itemCallbackFunction(position: Int) {
        when (position) {
            0 -> Log.e("HoangPV", ": ahihi1")
            1 -> {
                val manageScheduleFragment = ManageScheduleFragment()
                val transaction = requireActivity().supportFragmentManager.beginTransaction()
                transaction.replace(R.id.home_owner_container, manageScheduleFragment, "TAG")
                    .addToBackStack("TAG").commit()

            }
            2 -> Log.e("HoangPV", ": ahihi2")
            3 -> Log.e("HoangPV", ": ahihi3")
        }
    }




}