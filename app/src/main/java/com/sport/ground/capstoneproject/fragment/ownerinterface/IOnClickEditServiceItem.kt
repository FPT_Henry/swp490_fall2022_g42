package com.sport.ground.capstoneproject.fragment.ownerinterface

import android.widget.EditText
import com.sport.ground.capstoneproject.model.Service

interface IOnClickEditServiceItem {
    fun decreaseAmountCallBack(position: Int, edtAmount: EditText, service: Service)
    fun increaseAmountCallBack(position: Int, edtAmount: EditText, service: Service)
}