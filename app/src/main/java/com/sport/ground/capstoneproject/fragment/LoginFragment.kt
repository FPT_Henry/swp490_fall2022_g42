package com.sport.ground.capstoneproject.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.sport.ground.capstoneproject.R
import com.sport.ground.capstoneproject.model.User


class LoginFragment : Fragment() {
    // TODO: Rename and change types of parameters


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }
}
//    private val args: LoginFragmentArgs by lazy {
//        LoginFragmentArgs.fromBundle(requireArguments())
//    }

//    val args: LoginFragmentArgs by navArgs()

//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//        initAction()
//        receiveDataFromWelcomeFragment()
//        goFragmentRegister()
//    }

//    private fun goFragmentRegister() {
//        tv_go_register.setOnClickListener {
//            val action = LoginFragmentDirections.actionLoginFragmentToRegisterFragment()
//            findNavController().navigate(action)
//    }
//}

//private fun receiveDataFromWelcomeFragment() {
//        val user = args.userRegister
//        edt_userName.setText(user.username)
//        edt_password.setText(user.password)
//}

//@SuppressLint("SetTextI18n")
//private fun initAction() {
//        btn_login?.setOnClickListener {
//            val username = edt_userName.text.toString()
//            val password = edt_password.text.toString()
//            val user = User(username, password)
//            if(username != "" && password != ""){
//                val action = LoginFragmentDirections.actionLoginFragmentToWelcomeFragment(user)
//                findNavController().navigate(action)
//            }else{
//                text_login_alert.text = "You need to fill in all the information"
//            }
//


//}