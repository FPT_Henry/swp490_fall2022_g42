package com.sport.ground.capstoneproject.fragment.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.CheckBox
import android.widget.TextView
import com.sport.ground.capstoneproject.R
import com.sport.ground.capstoneproject.model.Schedule

class TimeSetAdapter(private val context: Context, private val schedules: List<Schedule>) :
    BaseAdapter() {

    override fun getCount(): Int = schedules.size


    override fun getItem(position: Int): Any = schedules[position]


    override fun getItemId(position: Int): Long = schedules[position].scheduleId.toLong()


    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        var view: View? = null
        if (view == null) {
            view =
                LayoutInflater.from(context)
                    .inflate(R.layout.item_edit_time_schedule, parent, false)
        }
        val schedule = getItem(position) as Schedule

        val tvTime: TextView? = view?.findViewById(R.id.tv_hour_item_schedule)
        val cbBooking: CheckBox? = view?.findViewById(R.id.cb_hour_item_schedule)
        val tvAmount: TextView? = view?.findViewById(R.id.tv_amount_item_Schedule)
        tvTime?.text = schedule.timeSlot
        tvAmount?.text = "${schedule.price} VND"
        cbBooking?.isChecked = schedule.isBooking
        return view
    }
}