package com.sport.ground.capstoneproject.servive

import okhttp3.Interceptor

class OAuthInterceptor( private val acceessToken: String):
    Interceptor {

    override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
        var request = chain.request()
        request = request.newBuilder().header("Authorization", " $acceessToken").build()

        return chain.proceed(request)
    }
}