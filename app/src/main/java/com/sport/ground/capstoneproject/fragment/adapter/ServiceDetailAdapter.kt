package com.sport.ground.capstoneproject.fragment.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.sport.ground.capstoneproject.R
import com.sport.ground.capstoneproject.model.ServiceDetail
import com.sport.ground.capstoneproject.model.SportGround

class ServiceDetailAdapter(
    private var context: Context,
    private var services: List<String>
) : BaseAdapter() {

    override fun getCount(): Int = services.size


    override fun getItem(position: Int): Any = services[position]


    override fun getItemId(position: Int): Long = services[position].toLong()


    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        var view: View? = null
        if (view == null) {
            view =
                LayoutInflater.from(context).inflate(R.layout.item_detail_service, parent, false)
        }
        val serviceDetail = getItem(position) as String

        val tvDot: TextView? = view?.findViewById(R.id.tv_dot_service_detail_item)
        val tvServiceDetailItem: TextView? = view?.findViewById(R.id.tv_service_detail_item)
        tvDot?.text = "-"
        tvServiceDetailItem?.text = serviceDetail
        return view
    }

}