package com.sport.ground.capstoneproject.fragment.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import com.sport.ground.capstoneproject.R
import com.sport.ground.capstoneproject.fragment.ownerinterface.IOnclickScheduleDetail
import com.sport.ground.capstoneproject.model.Schedule
import com.sport.ground.capstoneproject.model.SportGround

class ScheduleAdapter(
    private val schedules: List<Schedule>,
    private val IOnClickDetail: IOnclickScheduleDetail
) :
    BaseAdapter() {

    override fun getCount(): Int = schedules.size

    override fun getItem(position: Int): Any = schedules[position]

    override fun getItemId(position: Int): Long = schedules[position].scheduleId.toLong()

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        var view: View? = null
        if (view == null) {
            view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_schedule_ground, parent, false)
        }
        val schedule = getItem(position) as Schedule

        val tvTimeSlot: TextView? = view?.findViewById(R.id.tv_time_each_slot)
        val isBooking: CheckBox? = view?.findViewById(R.id.check_box_time)
        val tvDetail: TextView? = view?.findViewById(R.id.tv_slot_detail)
        tvDetail?.setOnClickListener {
            IOnClickDetail.itemCallbackScheduleDetail(schedule.scheduleId)
        }
        tvTimeSlot?.text = schedule.timeSlot
        isBooking?.isChecked = schedule.isBooking
        return view
    }
}