package com.sport.ground.capstoneproject.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
data class SportGround(
    var groundId: Int,
    var groundName: String,
    var groundType: String,
    var address: String,
    var manager: String,
    var image:Int,
    var slot: Int
): Parcelable, Serializable