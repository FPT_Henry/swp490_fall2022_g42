package com.sport.ground.capstoneproject.model

data class DefaultResponse(val accessToken: String)
