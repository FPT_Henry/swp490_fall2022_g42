package com.sport.ground.capstoneproject.servive

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ServiceBuilder {
//    val client =  OkHttpClient.Builder()
//        .addInterceptor(OAuthInterceptor( "---ACCESS---TOKEN---"))
//        .build()
//

    //http://apilayer.net/api/live?access_key=843d4d34ae72b3882e3db642c51e28e6&currencies=VND&source=USD&format=1
    val gson = GsonBuilder()
        .setLenient()
        .create()


        private val client = OkHttpClient.Builder().build()
    private val retrofit = Retrofit.Builder()
//        .baseUrl("http://192.168.1.21:8080//api/v1/auth/")  //192.168.1.21
        .baseUrl("http://apilayer.net/")  //192.168.1.21
        .addConverterFactory(GsonConverterFactory.create(gson))
        .client(client)
        .build()

    fun<T> buildService(service: Class<T>): T{
        return retrofit.create(service)
    }
}