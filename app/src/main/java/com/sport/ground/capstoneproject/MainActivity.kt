package com.sport.ground.capstoneproject

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.navigation.ui.AppBarConfiguration
import androidx.viewpager2.widget.ViewPager2
import com.sport.ground.capstoneproject.databinding.ActivityHomeBaseOwnerBinding
import com.sport.ground.capstoneproject.databinding.ActivityMainBinding
import com.sport.ground.capstoneproject.fragment.owner.HomeOwnerFragment
import com.sport.ground.capstoneproject.fragment.owner.adapterviewpager.ViewPagerAdapter

class MainActivity : AppCompatActivity() {

//    private lateinit var binding: ActivityHomeBaseOwnerBinding
    private lateinit var binding: ActivityMainBinding
    private lateinit var appBarConfiguration: AppBarConfiguration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        binding = ActivityHomeBaseOwnerBinding.inflate(layoutInflater)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

//        actionBottomNav()
//        actionLeftMenu()

    }
//
//    private fun actionLeftMenu() {
//        setSupportActionBar(binding.appBarMain.toolbar)
//        val toggle = ActionBarDrawerToggle(
//            this,
//            binding.drawerLayout,
//            binding.appBarMain.toolbar,
//            R.string.nav_drawer_open,
//            R.string.nav_drawer_close
//        )
//        binding.drawerLayout.addDrawerListener(toggle)
//        toggle.syncState()
//        binding.navView.setNavigationItemSelectedListener { item ->
//            Log.e("HoangPV", ": ${item.itemId}")
//
//            /**
//             * Called when an item in the navigation menu is selected.
//             *
//             * @param item The selected item
//             * @return true to display the item as the selected item
//             */
//            when (item.itemId) {
//                R.id.nav_light -> Toast.makeText(this@MainActivity, "Light", Toast.LENGTH_SHORT)
//                    .show()
//                R.id.nav_contact -> Toast.makeText(this@MainActivity, "Contact", Toast.LENGTH_SHORT)
//                    .show()
//                R.id.nav_language -> Toast.makeText(
//                    this@MainActivity,
//                    "Language",
//                    Toast.LENGTH_SHORT
//                ).show()
//                R.id.nav_rate_app -> Toast.makeText(this@MainActivity, "Rate", Toast.LENGTH_SHORT)
//                    .show()
//                R.id.nav_introduce -> Toast.makeText(
//                    this@MainActivity,
//                    "Introduce",
//                    Toast.LENGTH_SHORT
//                ).show()
//                else -> Toast.makeText(this@MainActivity, "None", Toast.LENGTH_SHORT).show()
//            }
//            binding.drawerLayout.closeDrawer(GravityCompat.START)
//            true
//        }
//    }
//
//
//    override fun onBackPressed() {
//        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
//            binding.drawerLayout.closeDrawer(GravityCompat.START)
//        } else {
//            super.onBackPressed()
//        }
//    }
//
//    override fun onCreateOptionsMenu(menu: Menu): Boolean {
//        menuInflater.inflate(R.menu.menu_setting, menu)
//        return true
//    }
//
//    private fun actionBottomNav() {
//        val pager2Adapter = ViewPagerAdapter(this)
//        binding.viewPager2.adapter = pager2Adapter
//        val zoomOutPage = ZoomOutPageTransformer()
//        binding.viewPager2.setPageTransformer(zoomOutPage)
//        binding.viewPager2.isUserInputEnabled = false;
//        binding.viewPager2.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
//            override fun onPageSelected(position: Int) {
//                super.onPageSelected(position)
//                when (position) {
//                    0 -> binding.bottomNav.menu.findItem(R.id.action_home).isChecked = true
//                    1 -> binding.bottomNav.menu.findItem(R.id.action_history).isChecked = true
//                    2 -> binding.bottomNav.menu.findItem(R.id.action_chat).isChecked = true
//                    3 -> binding.bottomNav.menu.findItem(R.id.action_notification).isChecked = true
//                    4 -> binding.bottomNav.menu.findItem(R.id.action_profile).isChecked = true
//                }
//            }
//        })
//
//        binding.bottomNav.setOnItemSelectedListener { menuItem ->
//            when (menuItem.itemId) {
//                R.id.action_home -> binding.viewPager2.currentItem = 0
//                R.id.action_history -> binding.viewPager2.currentItem = 1
//                R.id.action_chat -> binding.viewPager2.currentItem = 2
//                R.id.action_notification -> binding.viewPager2.currentItem = 3
//                R.id.action_profile -> binding.viewPager2.currentItem = 4
//            }
//            true
//        }
//    }


}