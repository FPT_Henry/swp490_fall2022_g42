package com.sport.ground.capstoneproject.fragment.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.sport.ground.capstoneproject.R
import com.sport.ground.capstoneproject.fragment.ownerinterface.IOnClickServiceItem
import com.sport.ground.capstoneproject.model.Schedule

class ServiceSetAdapter(
    private val iOnClick: IOnClickServiceItem,
    private val slots: List<String>
) :
    BaseAdapter() {

    override fun getCount(): Int = slots.size


    override fun getItem(position: Int): Any = slots[position]

    override fun getItemId(position: Int): Long = slots[position].toLong()


    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        var view: View? = null
        if (view == null) {
            view =
                LayoutInflater.from(parent?.context)
                    .inflate(R.layout.item_edit_service_schedule, parent, false)
        }
        val serviceDetail = getItem(position) as String

        val tvSlot: TextView? = view?.findViewById(R.id.tv_item_time_booked)
        val btnSelection: TextView? = view?.findViewById(R.id.btn_selection_service_item)
        tvSlot?.text = serviceDetail
        btnSelection?.setOnClickListener {
            iOnClick.itemCallbackService(position)
        }

        return view
    }
}